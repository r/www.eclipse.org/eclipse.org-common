<?php

/**
 * *****************************************************************************
 * Copyright (c) 2006 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Denis Roy (Eclipse Foundation)- initial API and implementation
 * *****************************************************************************
 */
require_once ("link.class.php");
class Nav {

  # *****************************************************************************
  #
  # nav.class.php
  #
  # Author: Denis Roy
  # Date 2004-09-11
  #
  # Description: Functions and modules related to Navbar objects
  #
  # HISTORY:
  #
  # *****************************************************************************

  private $LinkList = array();

  private $HTMLBlock = "";

  private $type = "";

  /**
   * Store html to use after <aside>
   *
   * @var string
   */
  private $HTMLBlockSuffix = "";

  # Main constructor
  function __construct() {
    $www_prefix = "";

    global $App;

    if (isset($App)) {
      $www_prefix = $App->getWWWPrefix();
    }
  }

  /**
   * Get $HTMLBlockSuffix
   *
   * @return string
   */
  public function getHtmlBlockSuffix() {
    return $this->HTMLBlockSuffix;
  }

  /**
   * Set $HTMLBlockSuffix
   *
   * @return string
   */
  public function setHtmlBlockSuffix($html = "") {
    if (!empty($html) && is_string($html)) {
      $this->HTMLBlockSuffix = $html;
    }
    return $this->HTMLBlockSuffix;
  }


  function getLinkList() {
    return $this->LinkList;
  }

  function setLinkList($_LinkList) {
    $this->LinkList = $_LinkList;
  }

  function getHTMLBlock() {
    return $this->HTMLBlock;
  }

  function setHTMLBlock($html) {
    $this->HTMLBlock = $html;
  }

  /**
   * Get the Nav type
   *
   * @return string
   */
  function getType() {
    return $this->type;
  }

  /**
   * Set the Nav type
   *
   * @param string $type
   */
  function setType($type) {
    $this->type = $type;
  }

  function addCustomNav($_Text, $_URL, $_Target, $_Level, $_CSS = NULL) {
    if ($_Level == "") {
      $_Level = 0;
    }
    $Link = new Link($_Text, $_URL, $_Target, $_Level, $_CSS);

    # Add incoming Nav Item
    $this->LinkList[count($this->LinkList)] = $Link;
  }

  function addNavSeparator($_Text, $_URL) {
    $Link = new Link($_Text, $_URL, "__SEPARATOR", 1);

    # Add incoming Nav Item
    $this->LinkList[count($this->LinkList)] = $Link;
  }

  # remove list function from projectInfoList class, and put it here where it
  # really belongs
  function orderNavList($_InfoList) {
    if (!function_exists("cmp_navobj")) {

      function cmp_navobj($a, $b) {
        $al = $a->order;
        $bl = $b->order;
        if ($al == $bl) {
          return 0;
        }
        return ($al > $bl) ? +1 : -1;
      }
    }
    usort($_InfoList, "cmp_navobj");
    return $_InfoList;
  }

  function getLinkCount() {
    return count($this->LinkList);
  }

  function getLinkAt($_Pos) {
    if ($_Pos < $this->getLinkCount()) {
      return $this->LinkList[$_Pos];
    }
  }

}
