<?php

/**
 * *****************************************************************************
 * Copyright (c) 2016, 2023 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Christopher Guindon (Eclipse Foundation) - Initial implementation
 * Olivier Goulet <olivier.goulet@eclipse-foundation>
 * *****************************************************************************
 */

require_once 'quicksilver.class.php';
/**
 *
 */
class Astro extends Quicksilver {

  /**
   * Constructor.
   */
  public function __construct($App = NULL) {
    parent::__construct($App);
    $this->setTheme('astro');
    $this->setAttributes('script-theme-main-js', $this->getThemeUrl('solstice') . 'public/javascript/astro.min.js?var=0.0.272', 'src');

    // Header
    $this->setAttributes('header-wrapper', 'header', 'id');

    // Footer links
    $this->footer_links = array();

    $this->setFooterLinks('about', 'About', $this->getBaseUrl() . 'org/', 'region_1', 1);
    $this->setFooterLinks('projects', 'Projects', 'https://projects.eclipse.org/', 'region_1', 2);
    $this->setFooterLinks('collaborations', 'Collaborations', $this->getBaseUrl() . 'collaborations/', 'region_1', 3);
    $this->setFooterLinks('membership', 'Membership', $this->getBaseUrl() . 'membership/', 'region_1', 4);
    $this->setFooterLinks('sponsor', 'Sponsor', $this->getBaseUrl() . 'sponsor/', 'region_1', 5);

    $this->setFooterLinks('privacy_policy', 'Privacy Policy', $this->getBaseUrl() . 'legal/privacy.php', 'region_2', 1);
    $this->setFooterLinks('terms_of_use', 'Terms of Use', $this->getBaseUrl() . 'legal/termsofuse.php', 'region_2', 2);
    $this->setFooterLinks('compliance', 'Compliance', $this->getBaseUrl() . 'legal/compliance/', 'region_2', 3);
    $this->setFooterLinks('code_of_conduct', 'Code of Conduct', $this->getBaseUrl() . 'org/documents/Community_Code_of_Conduct.php', 'region_2', 4);
    $this->setFooterLinks('legal_resources', 'Legal Resources', $this->getBaseUrl() . 'legal/', 'region_2', 5);

    $this->setFooterLinks('report_vulnerability', 'Report a Vulnerability', $this->getBaseUrl() . 'security/', 'region_3', 1);
    $this->setFooterLinks('service_status', 'Service Status', 'https://www.eclipsestatus.io/', 'region_3', 2);
    $this->setFooterLinks('contact', 'Contact', $this->getBaseUrl() . 'org/foundation/contact.php', 'region_3', 3);
    $this->setFooterLinks('support', 'Support', $this->getBaseUrl() . '/projects/support/', 'region_3', 4);
  }

  /**
   * Get array of theme files if they exist.
   */
  public function getThemeFiles() {
    $App = $this->_getApp();
    $eclipse_org_common_root = $App->getBasePath();

    $files = array();
    $files['header'] = $eclipse_org_common_root . '/themes/solstice/header.php';
    $files['menu'] = $eclipse_org_common_root . '/themes/solstice/astro/menu.php';
    $files['nav'] = $eclipse_org_common_root . '/themes/solstice/astro/nav.php';
    $files['body'] = $eclipse_org_common_root . '/themes/solstice/body.php';
    $files['main_menu'] = $eclipse_org_common_root . '/themes/solstice/main_menu.php';
    $files['footer'] = $eclipse_org_common_root . '/themes/solstice/astro/footer.php';
    $files['footer-min'] = $eclipse_org_common_root . '/themes/solstice/footer-min.php';

    // Validate theme files.
    foreach ($files as $key => $template_files) {
      if (!file_exists($template_files)) {
        unset($files[$key]);
      }
    }

    return $files;
  }

  /**
   * Get page $html.
   */
  public function getHtml() {
    $nav_position = $this->getNavPosition();
    $nav_position = strtolower($nav_position);
    $content_col_classes = 'col-md-24';

    $nav_col_classes = '';
    if ($this->getThemeFile('nav') && $nav_position !== 'disabled') {
      $content_col_classes = 'col-md-18';
      $nav_col_classes = 'col-md-6';
    }

    if ($nav_position === 'left') {
      $content_col_classes .= ' col-md-push-6';
      $nav_col_classes .= ' col-md-pull-18';
    }
    // Define the body content.
    $body = $this->getDeprecatedMessage();
    $body .= $this->getHeaderNav();
    $body .= $this->getSystemMessages();
    $body .= $this->getThemeVariables('main_container_html');
    $body .= $this->html;

    // Set a different body if there's a sidebar.
    $nav_html = "";
    if ($nav_position !== 'disabled' && $nav = $this->getThemeFile('nav')) {
      $nav_html = '<div class="' . $nav_col_classes . ' main-col-sidebar-nav">' . $nav . '</div>';
    }

    // Define the html.
    return <<<EOHTML
    <main {$this->getAttributes('main')}>
      <div {$this->getAttributes('main-container')}>
        <div class="row">
          <div class="{$content_col_classes} main-col-content">
            <div class="novaContent" id="novaContent">
              <div class="row">
                {$this->getBreadcrumbHtml()}
              </div>
              <div class=" main-col-content">
                {$body}
              </div>
            </div>
          </div>
          {$nav_html}
        </div>
      </div>
    </main>
EOHTML;
  }

  /**
   * Get the default astro menu
   *
   * This is stubbed
   *
   * @return Menu
   */
  protected function _getMenuDefault() {
    $App = $this->_getApp();
    if (!class_exists('Menu')) {
      require_once ($App->getBasePath() . '/system/menu.class.php');
    }

    $Menu = new Menu();
    $Menu->setMenuItemList(array());
    $Menu->addMenuItem('Projects', '#', '_self');
    $Menu->addMenuItem('Supporters', '#', '_self');
    $Menu->addMenuItem('Collaborations', '#', '_self');
    $Menu->addMenuItem('Resources', '#', '_self');
    $Menu->addMenuItem('The Foundation', '#', '_self');

    return $Menu;
  }

  public function isDefaultMenu() {
    $main_menu = $this->_getMenu();
    $default_menu = new Menu();

    $is_default_menu = FALSE;
    if ($main_menu->getMenuArray() == $default_menu->getMenuArray()) {
      $is_default_menu = TRUE;
    }

    return $is_default_menu;
  }

  private function getMenuItemId($item) {
    return str_replace(' ', '-', strtolower($item->getText())) . '-menu';
  }

  public function getMenu() {
    $main_menu = $this->_getMenu()->getMenuArray();

    if ($this->isDefaultMenu()) {
      // Menu did not change, let's set the default Solstice menu.
      $main_menu = $this->_getMenuDefault()->getMenuArray();
    }

    $items = array();

    foreach ($main_menu as $item) {
      $caption = $item->getText();
      if ($this->isDefaultMenu()) {
        $items[] = <<<EOHTML
          <li class="navbar-nav-links-item">
            <button
              class="nav-link-js btn-link link-unstyled"
              type="button"
              aria-expanded="false"
              data-menu-target="{$this->getMenuItemId($item)}"
            >
              {$caption}
            </button>
          </li>
EOHTML;
      } else {
        $items[] = <<<EOHTML
          <li class="navbar-nav-links-item">
            <a class="link-unstyled" href="{$item->getURL()}" target="{$item->getTarget()}">
              {$caption}
            </a>
          </li>
EOHTML;
      }
    }

    return implode('', $items);
  }

  public function getMobileMenu() {
    $main_menu = $this->_getMenu()->getMenuArray();

    $items = array();

    // Temporarily only support non-default
    if ($this->isDefaultMenu()) {
      return implode('', $items);
    }

    foreach ($main_menu as $item) {
      $items[] = <<<EOHTML
        <li class="">
          <a class="mobile-menu-item" href="{$item->getURL()}" target="{$item->getTarget()}">
            {$item->getText()}
          </a>
        </li>
EOHTML;
    }

    return implode('', $items);
  }

  public function getToolbarLinks() {
    $more_links = '<li>'. $this->getSessionVariables('manage_cookies') .'</li>';
    $dropdown = $this->getSessionVariables('dropdown');

    if (!empty($dropdown)) {
      $more_links = '
                  <li>'. $this->getSessionVariables('manage_account') .'</li>
                  <li>'. $this->getSessionVariables('view_account') .'</li>
                  <li class="divider"></li>
                  <li>'. $this->getSessionVariables('manage_cookies') .'</li>
                  '.$this->getSessionVariables('logout');
    }

    return <<<EOHTML
      <li>{$this->getSessionVariables('user_login')}</li>
      {$more_links}
EOHTML;
  }

  /**
   * Get default variables for CFA
   *
   * @return array
   */
  protected function _getCfaButtonDefault() {
    $default['class'] = 'btn btn-primary';
    $default['href'] = '/downloads/';
    $default['text'] = 'Download';
    return $default;
  }

  /**
   * Get Html of Featured Footer
   */
  public function getFeaturedStoryFooter() {
   return '<div class="eclipsefdn-featured-footer featured-footer" data-publish-target="eclipse_org"><div class="container featured-container"></div></div>';
  }

  /**
   * Get Html of Footer Region 1
   */
  public function getFooterRegion1() {
    return <<<EOHTML
      <div class="menu-heading">Eclipse Foundation</div>
      <ul class="nav">
        {$this->getFooterLinks('region_1')}
      </ul>
EOHTML;
  }

  /**
   * Get Html of Footer Region 2
   */
  public function getFooterRegion2() {
    return <<<EOHTML
      <div class="menu-heading">Legal</div>
      <ul class="nav">
        {$this->getFooterLinks('region_2')}
        <li>
          <a href="#" class="toolbar-manage-cookies">Manage Cookies</a>
        </li>
      </ul>
EOHTML;
  }

  /**
   * Get Html of Footer Region 2
   */
  public function getFooterRegion3() {
    return <<<EOHTML
      <div class="menu-heading">More</div>
      <ul class="nav">
        {$this->getFooterLinks('region_3')}
      </ul>
EOHTML;
  }
}
