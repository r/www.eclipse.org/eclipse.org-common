<?php
/**
 * Copyright (c) 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - initial API and implementation
 * Christopher Guindon (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */

//if name of the file requested is the same as the current file, the script will exit directly.
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}

require_once("DownloadsProject.class.php");
require_once("eclipseInstaller.php");

class Downloads extends DownloadsProject {

  protected $projects = array();

  protected $banner_ads = array();

  protected $promo_ad_type = "default";

  private $Installer = NULL;

  public function __construct() {

    $this->Installer = new EclipseInstaller();
    $this->Installer->setInstallerLayout('layout_a');

    // Set the initial content of the Projects array
    $this->_setProjectsArray();

    // PROJECTS

    $Project = new DownloadsProject();
    $Project->setTitle('Eclipse Temurin');
    $Project->setDescription('The Eclipse Temurin™ project provides high-quality, TCK certified OpenJDK runtimes and associated technology for use across the Java™ ecosystem.');
    $Project->setLogo('assets/public/images/logo-temurin.png');
    $Project->setProjectType($this->projects['tool_platforms']['title']);
    $Project->setDownloadUrl64Bit('https://adoptium.net/');
    $Project->setLearnMoreUrl('https://projects.eclipse.org/projects/adoptium.temurin');
    $Project->setProjectsAttributes('image','height','70');
    $Project->setProjectsAttributes('container','class','col-md-18 col-md-offset-3 downloads-items');
    $this->newProject($Project);

    $Project = new DownloadsProject();
    $Project->setTitle('Eclipse Che');
    $Project->setDescription('Eclipse Che is a developer workspace server and cloud IDE.');
    $Project->setLogo('assets/public/images/logo-che.png');
    $Project->setProjectType($this->projects['other_tools']['title']);
    $Project->setDownloadUrl64Bit('https://www.eclipse.org/che/getting-started/download/');
    $Project->setLearnMoreUrl('https://eclipse.org/che');
    $Project->setProjectsAttributes('image','height','50');
    $Project->setProjectsAttributes('container','class','col-md-12 downloads-items');
    $this->newProject($Project);

    $Project = new DownloadsProject();
    $Project->setTitle('Theia');
    $Project->setDescription('An open, flexible and extensible cloud & desktop IDE platform');
    $Project->setLogo('assets/public/images/logo-theia.png');
    $Project->setProjectType($this->projects['other_tools']['title']);
    $Project->setDownloadUrl64Bit('https://theia-ide.org');
    $Project->setLearnMoreUrl('https://theia-ide.org/#gettingstarted');
    $Project->setProjectsAttributes('image','height','25');
    $Project->setProjectsAttributes('container','class','col-md-12 downloads-items');
    $this->newProject($Project);

    $Project = new DownloadsProject();
    $Project->setTitle('Jetty');
    $Project->setDescription('Eclipse Jetty provides a web server and javax.servlet container.');
    $Project->setLogo('assets/public/images/logo-jetty.png');
    $Project->setProjectType($this->projects['other_runtimes']['title']);
    $Project->setDownloadUrl64Bit('https://www.eclipse.org/jetty/download.html');
    $Project->setLearnMoreUrl('http://www.eclipse.org/jetty/');
    $Project->setProjectsAttributes('container','class','col-md-12 downloads-items');
    $this->newProject($Project);

    $Project = new DownloadsProject();
    $Project->setTitle('GlassFish');
    $Project->setDescription('Eclipse GlassFish provides a complete application server which serves as a compatible implementation for the Jakarta EE specification.');
    $Project->setLogo('assets/public/images/logo-glassfish.png');
    $Project->setProjectType($this->projects['other_runtimes']['title']);
    $Project->setDownloadUrl64Bit('https://projects.eclipse.org/projects/ee4j.glassfish/downloads');
    $Project->setLearnMoreUrl('https://projects.eclipse.org/projects/ee4j.glassfish');
    $Project->setProjectsAttributes('image','height','75');
    $Project->setProjectsAttributes('container','class','col-md-12 downloads-items');
    $this->newProject($Project);
  }

  /**
   * Set the initial content of the Projects Array
   */
  private function _setProjectsArray() {
    $this->projects = array(
      'tool_platforms' => array(
        'title' => 'OpenJDK Runtimes',
        'items' => array(),
      ),
      'other_tools' => array(
        'title' => 'Other Tools',
        'items' => array(),
      ),
      'other_runtimes' => array(
        'title' => 'Other Runtimes',
        'items' => array(),
      ),
    );
  }

  /**
   * Adds a new Project to a specific array
   *
   * @param $Project object
   *
   * @return bool
   */
  public function newProject($Project = NULL) {

    // Prevent an invalid project to be entered
    if (!$valid = $this->validProject($Project)) {
      return FALSE;
    }

    // Add each projects to their own category
    foreach ($this->projects as $key => $category) {
      if ($Project->getProjectType() == $category['title']) {
        $this->projects[$key]['items'][] = $Project;
      }
    }
    return TRUE;
  }

  /**
   * Makes the banner ad disapear
   *
   * @param $hide - bool
   */
  public function hideBannerAd($hide = TRUE) {
    if (filter_var($hide, FILTER_VALIDATE_BOOLEAN)) {
      $this->hide_banner_ad = $hide;
    }
  }

  /**
   * Returns a promo ad block depending on the ad type
   *
   * @deprecated
   * @return string
   */
  public function getPromoAd() {
    return "";
  }

  /**
   * Returns the HTML output of a list of projects
   *
   * @param $projects - array
   *
   * @return string
   */
  private function _projectsOutput($projects) {
    ob_start();
    foreach ($projects as $project) {
      include 'views/view.projects.php';
    }
    return ob_get_clean();
  }

  /**
   * Get all the projects
   *
   * @return string
   */
  public function getAllDownloadsProjects() {
    ob_start();
    foreach ($this->projects as $key =>$category) {
      include 'views/view.category.php';
    }
    return ob_get_clean();
  }

  /**
   * Get the list of projects depending on the specified category
   *
   * @param $category - string
   *
   * @return string
   */
  public function getProjectsList($category) {
    if (filter_var($category, FILTER_SANITIZE_STRING)) {
      return $this->_projectsOutput($this->projects[$category]['items']);
    }
  }
}
