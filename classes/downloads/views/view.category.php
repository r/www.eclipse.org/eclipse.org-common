<?php
/**
 * Copyright (c) 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eric Poirier (Eclipse Foundation) - initial API and implementation
 *   Christopher Guindon (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */

?>
<?php if ($key == 'tool_platforms') :?>
<div id="<?php print strtolower(str_replace(" ", "-", $category['title'])); ?>" class="downloads-section">
  <div class="container">
    
    <div class="row downloads-content-padding text-center">

      <?php if ($key == 'tool_platforms') :?>
        <!-- Installer -->
        <div class="col-md-12 col-sm-10 col-sm-offset-1 col-md-offset-0 padding-right-0 downloads-installer">
          <?php print $this->Installer->output('x86_64'); ?>
        </div>
      <?php endif;?>

      <div class="col-md-12 col-sm-10 col-sm-offset-2 col-md-offset-0">
        <h2><span class="downloads-title"><?php print $category['title']; ?></span></h2>
        <?php print $this->getProjectsList($key); ?>
      </div>
    </div>
  </div>
</div>
<?php else: ?>

  <?php if ($key == 'other_tools') :?>
    <div class="container">
    <div class="row downloads-content-padding text-center">
      <div class="col-sm-12">
        <h2><span class="downloads-title"><?php print $category['title']; ?></span></h2>
        <?php print $this->getProjectsList($key); ?>
      </div>
  <?php endif; ?>

  <?php if ($key == 'other_runtimes') :?>
      <div class="col-sm-12">
        <h2><span class="downloads-title"><?php print $category['title']; ?></span></h2>
        <?php print $this->getProjectsList($key); ?>
      </div>
    </div>
  </div>
  <?php endif; ?>


<?php endif;?>

<?php if ($key == 'other_runtimes') :?>
  <div class="downloads-section">
    <div class="eclipsefdn-promo-content text-center" data-ad-format="ads_leaderboard" data-ad-publish-to="eclipse_org_downloads"></div>
  </div>
<?php endif; ?>