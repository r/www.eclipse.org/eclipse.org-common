<?php

/**
 * ********************************************************************
 * Copyright (c) 2021 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Christopher Guindon (Eclipse Foundation) - initial API and implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 * ********************************************************************
 */

require_once(realpath(dirname(__FILE__) . "/../../system/app.class.php"));
require_once("director.class.php");

class DirectorsList {

  /**
   * List of directors
   */
  private $directors = array();

  /**
   * The singleton class instance is stored in a static field.
   */
  private static $_instance = null;

  /**
   * The class constructor is private to prevent direct
   * construction calls with the `new` operator.
   */
  private function __construct() {
  }

  /**
   * Get the singleton instance
   */
   public static function getInstance() {
    if (!self::$_instance) {
      self::$_instance = new DirectorsList();
    }
    return self::$_instance;
  }

  /**
   * Get List of US Board Directors
   *
   * @return string
   */
  public function getBoardDirectorsUS() {
    $included_relations = array("BRUS", "CBUS", "CMUS", "FRUS");
    return $this->_output($included_relations);
  }

  /**
   * Get List of BE (AISBL) Board Directors
   *
   * @return string
   */
  public function getBoardDirectorsBE() {
    $included_relations = array("BRBE", "CBBE", "CMBE");
    return $this->_output($included_relations);
  }

  /**
   * Fetch, sort and output list of directors
   */
  private function _output($included_relations) {
    $directors_list = array();
    $directors = $this->_fetchBoardMembers();
    foreach ($directors as $Director) {
      if (in_array($Director->getRelation(), $included_relations)) {
        $directors_list[$Director->getArrayKey()] = $Director;
      }
    }

    // sort by last_name
    ksort($directors_list);

    $html = '';
    foreach ($directors_list as $Director) {
      $html .=  $Director->outputDirector();
    }

    return $html;
  }

  /**
   * Fetch board members from Foundation DB
   *
   * @return array
   * */
  private function _fetchBoardMembers() {
    if (empty($this->directors)) {
      $this->directors = array();
      $App =  new App();

      // Members with company relationships
      $sql = 'SELECT p.FName, p.LName, o.Name1 as OrganizationName, o.OrganizationID, oc.Relation
            FROM  People as p, OrganizationContacts as oc, Organizations as o
            WHERE p.PersonID = oc.PersonID
            AND (oc.Relation = "BRBE" or oc.Relation = "BRUS")
            AND oc.OrganizationID = o.OrganizationID';
      $result = $App->foundation_sql($sql);

      while ($row = mysql_fetch_assoc($result)) {
        $Director = new Director();
        $Director->setFirstName($row['FName'])
        ->setLastName($row['LName'])
        ->setOrganizationName($row['OrganizationName'])
        ->setOrganizationId($row['OrganizationID'])
        ->setRelation($row['Relation']);
        $this->director[] = $Director;
      }

      // Elected add-in provider reps
      $sql = "SELECT p.FName, p.LName, pr.Relation as Relation
            FROM  People as p, PeopleRelations as pr
            WHERE p.PersonID = pr.PersonID
            AND (pr.Relation = 'CBBE' OR pr.Relation = 'CMBE' OR pr.Relation = 'CBUS'
            OR pr.Relation = 'CMUS' OR pr.Relation = 'FRUS')";

      $result = $App->foundation_sql($sql);

      while ($row = mysql_fetch_assoc($result)) {
        $Director = new Director();
        $Director->setFirstName($row['FName'])
        ->setLastName($row['LName'])
        ->setRelation($row['Relation']);

        switch ($row['Relation']) {
          case 'CBBE':
          case 'CBUS':
            $Director->setOrganizationName("Committer Board Representative");
            break;
          case 'CMBE':
          case 'CMUS':
            $Director->setOrganizationName("Contributing Member Representative");
            break;
          case 'FRUS':
            $Director->setOrganizationName("Strategic Foundation Board Representative");
            break;
        }
        $this->director[] = $Director;
      }
    }
    return $this->director;
  }
}
