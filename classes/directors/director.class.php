<?php

/**
 * ********************************************************************
 * Copyright (c) 2021, 2023 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *  Christopher Guindon (Eclipse Foundation) - initial API and implementation
 *  Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 * ********************************************************************
 */

/**
 * Class Director.
 *
 * This class represents an Eclipse Foundation director.
 */
class Director { 
  private $first_name = '';
  private $last_name = '';
  private $organization_name = '';
  private $organization_id = '';
  private $relation = '';
  private $bio = '';

  public function setFirstName($first_name) {
    $this->first_name = ucwords(strtolower($first_name));
    return $this;
  }

  public function getFirstName() {
    return $this->first_name;
  }


  public function setLastName($last_name) {
    $this->last_name = ucwords(strtolower($last_name));
    return $this;
  }

  public function getLastName() {
    return  $this->last_name;
  }

  public function setOrganizationName($organization_name) {
    $this->organization_name = $organization_name;
    return $this;
  }

  public function getOrganizationName() {
    return $this->organization_name;
  }

  public function setOrganizationId($organization_id) {
    $this->organization_id = $organization_id;
    return $this;
  }

  public function getOrganizationId() {
    return $this->organization_id;
  }

  public function setRelation($relation) {
    $this->relation = strtoupper($relation);
    return $this;
  }

  public function getRelation() {
    return $this->relation;
  }

  /**
   * Replaces accented characters with their non-special character counterpart.
   *
   * @param string $str 
   *  A string which may contain accents.
   * 
   * @return string
   *  A string without accents.
   */
  private function replace_accents($str) {
    $str = htmlentities($str, ENT_COMPAT, "UTF-8");

    // Replace umlaut, acute, grave, circ and tilde accents from a string
    $str = preg_replace('/&([a-zA-Z])(uml|acute|grave|circ|tilde);/','$1',$str);
    
    return html_entity_decode($str);
  }

  /**
   * Get Bio from org repository
   *
   * @return string
   */
  private function getBio() {
    if (empty($this->bio)) {
      // Handle special cases where first or last name had multiple words
      $formatted_filename = str_replace(' ', '_', $this->getFirstName() . ' ' . $this->getLastName());

      // Replace periods in name
      $formatted_filename = str_replace('.', '', $formatted_filename);
      $formatted_filename = $this->replace_accents($formatted_filename);
      $formatted_filename = strtolower($formatted_filename);

      if (file_exists($file = $_SERVER['DOCUMENT_ROOT'] . '/org/foundation/boardbios/' . $formatted_filename . '.php')) {
        $this->bio = file_get_contents($file);
      }
    }

    return $this->bio;
  }

  /**
   * Get the html output of an Eclipse Foundation director.
   *
   * @return string
   *  The html output of an Eclipse Foundation director.
   */
  function outputDirector() {
    $is_aisbl_director = in_array($this->getRelation(), array("BRBE", "CBBE", "CMBE"));
    return $is_aisbl_director ? $this->outputAISBLDirector() : $this->outputIncDirector();
  }
  
  /**
   * Get the html output of an Eclipse Foundation AISBL director.
   *
   * @return string 
   *  The html output of an Eclipse Foundation AISBL director.
   */
  private function outputAISBLDirector() {
    return "<h3>{$this->getFirstName()} {$this->getLastName()} <small>{$this->getOrganizationName()}</small></h3>
    <div class=\"row\">
      <div class=\"col-sm-20\">{$this->getBio()}</div>
      <div class=\"col-sm-4\">{$this->getMemberLogo()}</div>
    </div>
    <hr>";
  }

  /**
   * Get the html output of an Eclipse Foundation, Inc. director.
   *
   * @return string 
   *  The html output of an Eclipse Foundation, Inc. director.
   */
  private function outputIncDirector() {
    return "<h3>{$this->getFirstName()} {$this->getLastName()}</h3>
    <div class=\"row\">
      <div class=\"col-sm-20\">{$this->getBio()}</div>
    </div>
    <hr>";
  }

  /**
   * Get sortable array key name value
   *
   * @return string
   */
  public function getArrayKey() {
    return ucwords($this->getLastName() . ', ' . $this->getFirstName());
  }

  /**
   * Get Member logo
   *
   * @return string
   *  The html output of a member logo
   */
  function getMemberLogo() {
    $logo = "";
    if ($this->getOrganizationId()) {
      $logo = "<img class='img img-responsive' src='https://membership.eclipse.org/api/organizations/" . $this->getOrganizationId() . "/web-logo' alt='" . $this->getOrganizationName() . "'>";
    }
    return $logo;
  }
}
