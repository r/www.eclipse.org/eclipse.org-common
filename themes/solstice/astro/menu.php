<?php
/**
 * Copyright (c) 2023 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Christopher Guindon (Eclipse Foundation) - Initial implementation
 *   Olivier Goulet (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
  $is_default_menu = $this->isDefaultMenu();
?>

<header <?php print $this->getAttributes('header-wrapper');?>>
  <div class="header-toolbar">
    <div class="container">
      <div class="header-toolbar-row">
        <div class="toolbar-btn toolbar-search-btn dropdown">
          <button class="dropdown-toggle" id="toolbar-search" type="button" data-toggle="dropdown" tabindex="0">
            <i class="fa fa-search fa-lg" aria-role="none"></i>
          </button>
          <div
            class="toolbar-search-bar-wrapper dropdown-menu dropdown-menu-right"
            aria-labelledby="toolbar-search"
          >
            <form action="https://www.eclipse.org/home/search" method="get">
              <div class="search-bar">
                <input class="search-bar-input" name="q" placeholder="Search" />
                <button>
                  <i class="fa fa-search" type="submit"></i>
                </button>
              </div>
            </form>
          </div>
        </div>
        <div class="toolbar-btn toolbar-user-menu-btn dropdown">
          <button class="dropdown-toggle" id="toolbar-user-menu" type="button" data-toggle="dropdown" tabindex="0">
            <i class="fa fa-user fa-lg"></i>
          </button>
          <ul class="toolbar-user-menu dropdown-menu dropdown-menu-right text-center"
            aria-labelledby="toolbar-user-menu">
            <?php print $this->getToolbarLinks() ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="header-navbar-wrapper">
    <div class="container">
      <div class="header-navbar">
        <a class="header-navbar-brand" href="<?php print $this->getBaseUrl();?>">
          <div class="logo-wrapper">
            <img src="/eclipse.org-common/themes/solstice/public/images/logo/eclipse-foundation-grey-orange.svg" alt="Eclipse Foundation"
              width="150" />
          </div>
        </a>
        <nav class="header-navbar-nav">
          <ul class="header-navbar-nav-links">
            <?php print $this->getMenu(); ?>
          </ul>
        </nav>
        <div class="header-navbar-end">
          <?php print $this->getCfaButton(); ?>
          <button class="mobile-menu-btn">
          <i class="fa fa-bars fa-xl"></i>
          </button>
        </div>
      </div>
    </div>
  </div>
  <nav class="mobile-menu hidden" aria-expanded="false">
    <ul>
      <?php if ($is_default_menu) :?>
        <li class="mobile-menu-dropdown">
          <a href="#" class="mobile-menu-item mobile-menu-dropdown-toggle" data-target="projects-menu">
          <span>Projects</span>
          <i class="fa fa-chevron-down" aria-hidden="true"></i>
          </a>
          <div class="mobile-menu-sub-menu-wrapper">
            <ul class="mobile-menu-sub-menu hidden" id="projects-menu">
              <li class="mobile-menu-dropdown">
                <a href="#" data-target="technologies-sub-menu"
                  class="mobile-menu-item mobile-menu-dropdown-toggle" aria-expanded="false">
                <span>Technologies</span>
                <i class="fa fa-chevron-down" aria-hidden="true"></i>
                </a>
                <div class="mobile-menu-sub-menu-wrapper">
                  <ul class="mobile-menu-sub-menu mobile-menu-links-menu hidden"
                    id="technologies-sub-menu">
                    <li>
                      <a class="mobile-menu-item" href="<?php print $this->getBaseUrl();?>topics/ide/">Developer Tools &amp; IDEs</a>
                    </li>
                    <li>
                      <a class="mobile-menu-item" href="<?php print $this->getBaseUrl();?>topics/cloud-native/">Cloud Native</a>
                    </li>
                    <li>
                      <a class="mobile-menu-item" href="<?php print $this->getBaseUrl();?>topics/edge-and-iot/">Edge &amp; IoT</a>
                    </li>
                    <li>
                      <a class="mobile-menu-item" href="<?php print $this->getBaseUrl();?>topics/automotive-and-mobility/">Automotive &amp; Mobility</a>
                    </li>
                  </ul>
                </div>
              </li>
              <li class="mobile-menu-dropdown">
                <a href="#" data-target="projects-sub-menu"
                  class="mobile-menu-item mobile-menu-dropdown-toggle" aria-expanded="false">
                <span>Projects</span>
                <i class="fa fa-chevron-down" aria-hidden="true"></i>
                </a>
                <div class="mobile-menu-sub-menu-wrapper">
                  <ul class="mobile-menu-sub-menu mobile-menu-links-menu hidden"
                    id="projects-sub-menu">
                    <li>
                      <a class="mobile-menu-item" href="https://projects.eclipse.org/">Project
                      Finder</a>
                    </li>
                    <li>
                      <a class="mobile-menu-item"
                        href="https://www.eclipse.org/projects/project_activity.php">Project
                      Activity</a>
                    </li>
                    <li>
                      <a class="mobile-menu-item" href="<?php print $this->getBaseUrl();?>projects/resources/">Project Resources</a>
                    </li>
                    <li>
                      <a class="mobile-menu-item"
                        href="<?php print $this->getBaseUrl();?>specifications/">Specifications</a>
                    </li>
                    <li>
                      <a class="mobile-menu-item"
                        href="<?php print $this->getBaseUrl();?>contribute/">Contribute</a>
                    </li>
                  </ul>
                </div>
              </li>
            </ul>
          </div>
        </li>
        <li class="mobile-menu-dropdown">
          <a href="#" class="mobile-menu-item mobile-menu-dropdown-toggle" data-target="supporters-menu">
          <span>Supporters</span>
          <i class="fa fa-chevron-down" aria-hidden="true"></i>
          </a>
          <div class="mobile-menu-sub-menu-wrapper">
            <ul class="mobile-menu-sub-menu hidden" id="supporters-menu">
              <li class="mobile-menu-dropdown">
                <a href="#" data-target="our-community-sub-menu"
                  class="mobile-menu-item mobile-menu-dropdown-toggle" aria-expanded="false">
                <span>Membership</span>
                <i class="fa fa-chevron-down" aria-hidden="true"></i>
                </a>
                <div class="mobile-menu-sub-menu-wrapper">
                  <ul class="mobile-menu-sub-menu mobile-menu-links-menu hidden"
                    id="our-community-sub-menu">
                    <li>
                      <a class="mobile-menu-item" href="<?php print $this->getBaseUrl();?>membership/exploreMembership.php">Our
                      Members</a>
                    </li>
                    <li>
                      <a class="mobile-menu-item"
                        href="<?php print $this->getBaseUrl();?>membership/">Member Benefits</a>
                    </li>
                    <li>
                      <a class="mobile-menu-item"
                        href="<?php print $this->getBaseUrl();?>membership/#tab-levels">Membership Levels &amp;
                      Fees</a>
                    </li>
                    <li>
                      <a class="mobile-menu-item"
                      href="<?php print $this->getBaseUrl();?>membership/#tab-membership">Membership
                      Application</a>
                    </li>
                    <li>
                      <a class="mobile-menu-item"
                      href="<?php print $this->getBaseUrl();?>membership/#tab-resources">Member Resources</a>
                    </li>
                    <li>
                      <a class="mobile-menu-item"
                        href="https://membership.eclipse.org/portal">Member Portal</a>
                    </li>
                  </ul>
                </div>
              </li>
              <li class="mobile-menu-dropdown">
                <a href="#" data-target="sponsorship-sub-menu"
                  class="mobile-menu-item mobile-menu-dropdown-toggle" aria-expanded="false">
                <span>Sponsorship</span>
                <i class="fa fa-chevron-down" aria-hidden="true"></i>
                </a>
                <div class="mobile-menu-sub-menu-wrapper">
                  <ul class="mobile-menu-sub-menu mobile-menu-links-menu hidden"
                    id="sponsorship-sub-menu">
                    <li>
                      <a class="mobile-menu-item"
                        href="https://newsroom.eclipse.org/">Sponsor</a>
                    </li>
                    <li>
                      <a class="mobile-menu-item" href="<?php print $this->getBaseUrl();?>org/corporate_sponsors/">Corporate Sponsorship</a>
                    </li>
                    <li>
                      <a class="mobile-menu-item" href="<?php print $this->getBaseUrl();?>sponsor/collaboration/">Sponsor a Collaboration</a>
                    </li>
                  </ul>
                </div>
              </li>
            </ul>
          </div>
        </li>
        <li class="mobile-menu-dropdown">
          <a href="#" class="mobile-menu-item mobile-menu-dropdown-toggle"
            data-target="collaborations-menu">
          <span>Collaborations</span>
          <i class="fa fa-chevron-down" aria-hidden="true"></i>
          </a>
          <div class="mobile-menu-sub-menu-wrapper">
            <ul class="mobile-menu-sub-menu hidden" id="collaborations-menu">
              <li class="mobile-menu-dropdown">
                <a href="#" data-target="industry-collaborations-sub-menu"
                  class="mobile-menu-item mobile-menu-dropdown-toggle" aria-expanded="false">
                <span>Industry Collaborations</span>
                <i class="fa fa-chevron-down" aria-hidden="true"></i>
                </a>
                <div class="mobile-menu-sub-menu-wrapper">
                  <ul class="mobile-menu-sub-menu mobile-menu-links-menu hidden"
                    id="industry-collaborations-sub-menu">
                    <li>
                      <a class="mobile-menu-item" href="<?php print $this->getBaseUrl();?>collaborations/">About Industry Collaborations</a>
                    </li>
                    <li>
                      <a class="mobile-menu-item" href="<?php print $this->getBaseUrl();?>org/workinggroups/explore.php">Current Collaborations</a>
                    </li>
                    <li>
                      <a class="mobile-menu-item" href="<?php print $this->getBaseUrl();?>org/workinggroups/about.php">About Working Groups</a>
                    </li>
                    <li>
                      <a class="mobile-menu-item" href="<?php print $this->getBaseUrl();?>collaborations/interest-groups/">About Interest Groups</a>
                    </li>
                  </ul>
                </div>
              </li>
              <li class="mobile-menu-dropdown">
                <a href="#" data-target="research-collaborations-sub-menu"
                  class="mobile-menu-item mobile-menu-dropdown-toggle" aria-expanded="false">
                <span>Research Collaborations</span>
                <i class="fa fa-chevron-down" aria-hidden="true"></i>
                </a>
                <div class="mobile-menu-sub-menu-wrapper">
                  <ul class="mobile-menu-sub-menu mobile-menu-links-menu hidden"
                    id="research-collaborations-sub-menu">
                    <li>
                      <a class="mobile-menu-item" href="<?php print $this->getBaseUrl();?>research/">Research @ Eclipse</a>
                    </li>
                  </ul>
                </div>
              </li>
            </ul>
          </div>
        </li>
        <li class="mobile-menu-dropdown">
          <a href="#" class="mobile-menu-item mobile-menu-dropdown-toggle" data-target="resources-menu">
          <span>Resources</span>
          <i class="fa fa-chevron-down" aria-hidden="true"></i>
          </a>
          <div class="mobile-menu-sub-menu-wrapper">
            <ul class="mobile-menu-sub-menu hidden" id="resources-menu">
              <li class="mobile-menu-dropdown">
                <a href="#" data-target="open-source-for-business-sub-menu"
                  class="mobile-menu-item mobile-menu-dropdown-toggle" aria-expanded="false">
                <span>Open Source for Business</span>
                <i class="fa fa-chevron-down" aria-hidden="true"></i>
                </a>
                <div class="mobile-menu-sub-menu-wrapper">
                  <ul class="mobile-menu-sub-menu mobile-menu-links-menu hidden"
                    id="open-source-for-business-sub-menu">
                    <li>
                      <a class="mobile-menu-item" href="<?php print $this->getBaseUrl();?>org/value/">Business Value of Open Source</a>
                    </li>
                    <li>
                      <a class="mobile-menu-item" href="<?php print $this->getBaseUrl();?>os4biz/services/">Professional Services</a>
                    </li>
                    <li>
                      <a class="mobile-menu-item" href="<?php print $this->getBaseUrl();?>os4biz/ospo/">Open Source Program Offices</a>
                    </li>
                  </ul>
                </div>
              </li>
              <li class="mobile-menu-dropdown">
                <a href="#" data-target="whats-happening-sub-menu"
                  class="mobile-menu-item mobile-menu-dropdown-toggle" aria-expanded="false">
                <span>What's Happening</span>
                <i class="fa fa-chevron-down" aria-hidden="true"></i>
                </a>
                <div class="mobile-menu-sub-menu-wrapper">
                  <ul class="mobile-menu-sub-menu mobile-menu-links-menu hidden"
                    id="whats-happening-sub-menu">
                    <li>
                      <a class="mobile-menu-item" href="https://newsroom.eclipse.org/">News</a>
                    </li>
                    <li>
                      <a class="mobile-menu-item" href="https://events.eclipse.org/">Events</a>
                    </li>
                    <li>
                      <a class="mobile-menu-item" href="<?php print $this->getBaseUrl();?>community/eclipse_newsletter/">Newsletter</a>
                    </li>
                    <li>
                      <a class="mobile-menu-item" href="https://newsroom.eclipse.org/news/press-releases">Press Releases</a>
                    </li>
                    <li>
                      <a class="mobile-menu-item" href="<?php print $this->getBaseUrl();?>org/foundation/eclipseawards/">Awards & Recognition</a>
                    </li>
                  </ul>
                </div>
              </li>
              <li class="mobile-menu-dropdown">
                <a href="#" data-target="developer-resources-sub-menu"
                  class="mobile-menu-item mobile-menu-dropdown-toggle" aria-expanded="false">
                <span>Developer Resources</span>
                <i class="fa fa-chevron-down" aria-hidden="true"></i>
                </a>
                <div class="mobile-menu-sub-menu-wrapper">
                  <ul class="mobile-menu-sub-menu mobile-menu-links-menu hidden"
                    id="developer-resources-sub-menu">
                    <li>
                      <a class="mobile-menu-item" href="https://www.eclipse.org/forums/">Forum</a>
                    </li>
                    <li>
                      <a class="mobile-menu-item" href="https://accounts.eclipse.org/mailing-list">Mailing Lists</a>
                    </li>
                    <li>
                      <a class="mobile-menu-item" href="https://blogs.eclipse.org/">Blogs &amp; Videos</a>
                    </li>
                    <li>
                      <a class="mobile-menu-item" href="<?php print $this->getBaseUrl();?>resources/marketplaces">Marketplaces</a>
                    </li>
                  </ul>
                </div>
              </li>
            </ul>
          </div>
        </li>
        <li class="mobile-menu-dropdown">
          <a href="#" class="mobile-menu-item mobile-menu-dropdown-toggle" data-target="the-foundation-menu">
          <span>The Foundation</span>
          <i class="fa fa-chevron-down" aria-hidden="true"></i>
          </a>
          <div class="mobile-menu-sub-menu-wrapper">
            <ul class="mobile-menu-sub-menu hidden" id="the-foundation-menu">
              <li class="mobile-menu-dropdown">
                <a href="#" data-target="the-foundation-about-sub-menu"
                  class="mobile-menu-item mobile-menu-dropdown-toggle" aria-expanded="false">
                <span>About</span>
                <i class="fa fa-chevron-down" aria-hidden="true"></i>
                </a>
                <div class="mobile-menu-sub-menu-wrapper">
                  <ul class="mobile-menu-sub-menu mobile-menu-links-menu hidden"
                    id="the-foundation-about-sub-menu">
                    <li>
                      <a class="mobile-menu-item" href="<?php print $this->getBaseUrl();?>org/">About the Eclipse Foundation</a>
                    </li>
                    <li>
                      <a class="mobile-menu-item" href="<?php print $this->getBaseUrl();?>org/governance">Board &amp; Governance</a>
                    </li>
                    <li>
                      <a class="mobile-menu-item" href="<?php print $this->getBaseUrl();?>org/foundation/staff.php">Staff</a>
                    </li>
                    <li>
                      <a class="mobile-menu-item" href="<?php print $this->getBaseUrl();?>org/services">Services</a>
                    </li>
                  </ul>
                </div>
              </li>
              <li class="mobile-menu-dropdown">
                <a href="#" data-target="legal-sub-menu"
                  class="mobile-menu-item mobile-menu-dropdown-toggle" aria-expanded="false"
                >
                  <span>Legal</span>
                  <i class="fa fa-chevron-down" aria-hidden="true"></i>
                </a>
                <div class="mobile-menu-sub-menu-wrapper">
                  <ul class="mobile-menu-sub-menu mobile-menu-links-menu hidden"
                    id="legal-sub-menu">
                    <li>
                      <a class="mobile-menu-item" href="<?php print $this->getBaseUrl();?>legal/">Legal Policies</a>
                    </li>
                    <li>
                      <a class="mobile-menu-item" href="<?php print $this->getBaseUrl();?>legal/privacy.php">Privacy Policy</a>
                    </li>
                    <li>
                      <a class="mobile-menu-item" href="<?php print $this->getBaseUrl();?>legal/termsofuse.php">Terms of Use</a>
                    </li>
                    <li>
                      <a class="mobile-menu-item" href="<?php print $this->getBaseUrl();?>legal/compliance/">Compliance</a>
                    </li>
                    <li>
                      <a class="mobile-menu-item" href="<?php print $this->getBaseUrl();?>legal/epl-2.0/">Eclipse Public License</a>
                    </li>
                  </ul>
                </div>
              </li>
              <li class="mobile-menu-dropdown">
                <a href="#" data-target="more-sub-menu"
                  class="mobile-menu-item mobile-menu-dropdown-toggle" aria-expanded="false">
                <span>More</span>
                <i class="fa fa-chevron-down" aria-hidden="true"></i>
                </a>
                <div class="mobile-menu-sub-menu-wrapper">
                  <ul class="mobile-menu-sub-menu mobile-menu-links-menu hidden"
                    id="more-sub-menu">
                    <li>
                      <a class="mobile-menu-item" href="https://newsroom.eclipse.org/news/press-releases">Press Releases</a>
                    </li>
                    <li>
                      <a class="mobile-menu-item" href="<?php print $this->getBaseUrl();?>careers/">Careers</a>
                    </li>
                    <li>
                      <a class="mobile-menu-item" href="<?php print $this->getBaseUrl();?>org/artwork/">Logos &amp; Artwork</a>
                    </li>
                    <li>
                      <a class="mobile-menu-item" href="<?php print $this->getBaseUrl();?>org/foundation/contact.php">Contact Us</a>
                    </li>
                  </ul>
                </div>
              </li>
            </ul>
          </div>
        </li>
      <?php else :?>
        <?php print $this->getMobileMenu(); ?>
      <?php endif ?>
    </ul>
  </nav>
  <?php if ($is_default_menu) :?>
    <div class="eclipsefdn-mega-menu">
      <div class="mega-menu-submenu container hidden" data-menu-id="projects-menu">
          <div class="mega-menu-submenu-featured-story">
              <p class="mega-menu-submenu-featured-story-heading">
                Projects
              </p>
              <p class="mega-menu-submenu-featured-story-text">
                The Eclipse Foundation is home to the Eclipse IDE, Jakarta EE, and hundreds of open source projects,
                including runtimes, tools, specifications, and frameworks for cloud and edge applications, IoT,
                AI, automotive, systems engineering, open processor designs, and many others.
              </p>
          </div>
          <div class="mega-menu-submenu-links-section">
              <div class="mega-menu-submenu-links">
                  <p class="menu-heading">Technologies</p>
                  <ul>
                      <li>
                          <a href="<?php print $this->getBaseUrl();?>topics/ide/">Developer Tools & IDEs</a>
                      </li>
                      <li>
                          <a href="<?php print $this->getBaseUrl();?>topics/cloud-native/">Cloud Native</a>
                      </li>
                      <li>
                          <a href="<?php print $this->getBaseUrl();?>topics/edge-and-iot/">Edge &amp; IoT</a>
                      </li>
                      <li>
                          <a href="<?php print $this->getBaseUrl();?>topics/automotive-and-mobility/">Automotive &amp; Mobility</a>
                      </li>
                  </ul>
              </div>
              <div class="mega-menu-submenu-links">
                  <p class="menu-heading">Projects</p>
                  <ul>
                      <li>
                          <a href="https://projects.eclipse.org/">Project Finder</a>
                      </li>
                      <li>
                          <a href="https://www.eclipse.org/projects/project_activity.php">Project Activity</a>
                      </li>
                      <li>
                          <a href="<?php print $this->getBaseUrl();?>projects/resources/">Project Resources</a>
                      </li>
                      <li>
                          <a href="<?php print $this->getBaseUrl();?>specifications/">Specifications</a>
                      </li>
                      <li>
                          <a href="<?php print $this->getBaseUrl();?>contribute/">Contribute</a>
                      </li>
                  </ul>
              </div>
          </div>
          <div class="mega-menu-submenu-ad-wrapper">
            <div
              class="eclipsefdn-mega-menu-promo-content mega-menu-promo-content"
              data-ad-format="ads_square"
              data-ad-publish-to="eclipse_org_home"
            >
            </div>
          </div>
      </div>
      <div class="mega-menu-submenu container hidden" data-menu-id="supporters-menu">
          <div class="mega-menu-submenu-featured-story">
              <p class="mega-menu-submenu-featured-story-heading">
                Supporters
              </p>
              <p class="mega-menu-submenu-featured-story-text">
              The Eclipse Foundation is an international non-profit
              association supported by our members, including industry
              leaders who value open source as a key enabler for their
              business strategies.
              </p>
          </div>
          <div class="mega-menu-submenu-links-section">
              <div class="mega-menu-submenu-links">
                  <p class="menu-heading">Membership</p>
                  <ul>
                      <li>
                          <a href="<?php print $this->getBaseUrl();?>membership/exploreMembership.php">Our Members</a>
                      </li>
                      <li>
                          <a href="<?php print $this->getBaseUrl();?>membership/">Member Benefits</a>
                      </li>
                      <li>
                          <a href="<?php print $this->getBaseUrl();?>membership/#tab-levels">Membership Levels &amp;
                              Fees</a>
                      </li>
                      <li>
                        <a href="<?php print $this->getBaseUrl();?>membership/#tab-membership">Membership Application</a>
                      </li>
                      <li>
                        <a href="<?php print $this->getBaseUrl();?>membership/#tab-resources">Member Resources</a>
                      </li>
                      <li>
                          <a href="https://membership.eclipse.org/portal">Member Portal</a>
                      </li>
                  </ul>
              </div>
              <div class="mega-menu-submenu-links">
                  <p class="menu-heading">Sponsorship</p>
                  <ul>
                      <li>
                          <a href="<?php print $this->getBaseUrl();?>sponsor/">Sponsor</a>
                      </li>
                      <li>
                          <a href="<?php print $this->getBaseUrl();?>org/corporate_sponsors/">Corporate Sponsorship</a>
                      </li>
                      <li>
                          <a href="<?php print $this->getBaseUrl();?>sponsor/collaboration/">Sponsor a Collaboration</a>
                      </li>
                  </ul>
              </div>
          </div>
          <div class="mega-menu-submenu-ad-wrapper">
            <div
              class="eclipsefdn-mega-menu-promo-content mega-menu-promo-content"
              data-ad-format="ads_square"
              data-ad-publish-to="eclipse_org_home"
            >
            </div>
          </div>
      </div>
      <div class="mega-menu-submenu container hidden" data-menu-id="collaborations-menu">
          <div class="mega-menu-submenu-featured-story">
              <p class="mega-menu-submenu-featured-story-heading">
                Collaborations
              </p>
              <p class="mega-menu-submenu-featured-story-text">
                Whether you intend on contributing to Eclipse technologies
                that are important to your product strategy, or simply want
                 to explore a specific innovation area with like-minded organizations,
                 the Eclipse Foundation is the open source home
                 for industry collaboration.
              </p>
          </div>
          <div class="mega-menu-submenu-links-section">
              <div class="mega-menu-submenu-links">
                  <p class="menu-heading">Industry Collaborations</p>
                  <ul>
                      <li>
                          <a href="<?php print $this->getBaseUrl();?>collaborations/">About Industry Collaborations</a>
                      </li>
                      <li>
                          <a
                              href="<?php print $this->getBaseUrl();?>org/workinggroups/explore.php">Current
                              Collaborations</a>
                      </li>
                      <li>
                          <a
                              href="<?php print $this->getBaseUrl();?>org/workinggroups/about.php">About
                              Working Groups</a>
                      </li>
                      <li>
                          <a
                              href="<?php print $this->getBaseUrl();?>collaborations/interest-groups/">About
                              Interest Groups</a>
                      </li>
                  </ul>
              </div>
              <div class="mega-menu-submenu-links">
                  <p class="menu-heading">Research Collaborations</p>
                  <ul>
                      <li>
                          <a href="<?php print $this->getBaseUrl();?>research/">Research @ Eclipse</a>
                      </li>
                  </ul>
              </div>
          </div>
          <div class="mega-menu-submenu-ad-wrapper">
            <div
              class="eclipsefdn-mega-menu-promo-content mega-menu-promo-content"
              data-ad-format="ads_square"
              data-ad-publish-to="eclipse_org_home"
            >
            </div>
          </div>
      </div>
      <div class="mega-menu-submenu container hidden" data-menu-id="resources-menu">
          <div class="mega-menu-submenu-featured-story">
              <p class="mega-menu-submenu-featured-story-heading">
                Resources
              </p>
              <p class="mega-menu-submenu-featured-story-text">
                The Eclipse community consists of individual developers and organizations
                spanning many industries. Stay up to date on our open source community
                and find resources to support your journey.
              </p>
          </div>
          <div class="mega-menu-submenu-links-section">
              <div class="mega-menu-submenu-links">
                  <p class="menu-heading">Open Source for Business</p>
                  <ul>
                      <li>
                          <a href="<?php print $this->getBaseUrl();?>org/value/">Business Value of Open Source</a>
                      </li>
                      <li>
                          <a href="<?php print $this->getBaseUrl();?>os4biz/services/">Professional Services</a>
                      </li>
                      <li>
                          <a href="<?php print $this->getBaseUrl();?>os4biz/ospo/">Open Source Program Offices</a>
                      </li>
                  </ul>
              </div>
              <div class="mega-menu-submenu-links">
                  <p class="menu-heading">What's Happening</p>
                  <ul>
                      <li>
                          <a href="https://newsroom.eclipse.org/">News</a>
                      </li>
                      <li>
                          <a href="https://events.eclipse.org/">Events</a>
                      </li>
                      <li>
                          <a href="<?php print $this->getBaseUrl();?>community/eclipse_newsletter">Newsletter</a>
                      </li>
                      <li>
                          <a href="https://newsroom.eclipse.org/news/press-releases">Press Releases</a>
                      </li>
                      <li>
                          <a href="<?php print $this->getBaseUrl();?>org/foundation/eclipseawards/">Awards & Recognition</a>
                      </li>
                  </ul>
              </div>
              <div class="mega-menu-submenu-links">
                  <p class="menu-heading">Developer Resources</p>
                  <ul>
                      <li>
                          <a href="https://www.eclipse.org/forums/">Forum</a>
                      </li>
                      <li>
                          <a href="https://accounts.eclipse.org/mailing-list">Mailing Lists</a>
                      </li>
                      <li>
                          <a href="https://blogs.eclipse.org/">Blogs &amp; Videos</a>
                      </li>
                      <li>
                          <a href="<?php print $this->getBaseUrl();?>resources/marketplaces">Marketplaces</a>
                      </li>
                  </ul>
              </div>
          </div>
          <div class="mega-menu-submenu-ad-wrapper">
            <div
              class="eclipsefdn-mega-menu-promo-content mega-menu-promo-content"
              data-ad-format="ads_square"
              data-ad-publish-to="eclipse_org_home"
            >
            </div>
          </div>
      </div>
      <div class="mega-menu-submenu container hidden" data-menu-id="the-foundation-menu">
          <div class="mega-menu-submenu-featured-story">
              <p class="mega-menu-submenu-featured-story-heading">
                The Foundation
              </p>
              <p class="mega-menu-submenu-featured-story-text">
              The Eclipse Foundation provides our global community of individuals
              and organizations with a mature, scalable, and vendor-neutral
              environment for open source software collaboration and innovation.
              </p>
          </div>
          <div class="mega-menu-submenu-links-section">
              <div class="mega-menu-submenu-links">
                  <p class="menu-heading">About</p>
                  <ul>
                      <li>
                          <a href="<?php print $this->getBaseUrl();?>org/">About the Eclipse Foundation</a>
                      </li>
                      <li>
                          <a href="<?php print $this->getBaseUrl();?>org/governance">Board &amp; Governance</a>
                      </li>
                      <li>
                          <a href="<?php print $this->getBaseUrl();?>org/foundation/staff.php">Staff</a>
                      </li>
                      <li>
                          <a href="<?php print $this->getBaseUrl();?>org/services">Services</a>
                      </li>
                  </ul>
              </div>
              <div class="mega-menu-submenu-links">
                  <p class="menu-heading">Legal</p>
                  <ul>
                      <li>
                          <a href="<?php print $this->getBaseUrl();?>legal/">Legal Policies</a>
                      </li>
                      <li>
                          <a href="<?php print $this->getBaseUrl();?>legal/privacy.php">Privacy Policy</a>
                      </li>
                      <li>
                          <a href="<?php print $this->getBaseUrl();?>legal/termsofuse.php">Terms of Use</a>
                      </li>
                      <li>
                          <a href="<?php print $this->getBaseUrl();?>legal/compliance/">Compliance</a>
                      </li>
                      <li>
                          <a href="<?php print $this->getBaseUrl();?>legal/epl-2.0/">Eclipse Public License</a>
                      </li>
                  </ul>
              </div>
              <div class="mega-menu-submenu-links">
                  <p class="menu-heading">More</p>
                  <ul>
                      <li>
                          <a href="https://newsroom.eclipse.org/news/press-releases">Press Releases</a>
                      </li>
                      <li>
                          <a href="<?php print $this->getBaseUrl();?>careers/">Careers</a>
                      </li>
                      <li>
                          <a href="<?php print $this->getBaseUrl();?>org/artwork/">Logos &amp; Artwork</a>
                      </li>
                      <li>
                          <a href="<?php print $this->getBaseUrl();?>org/foundation/contact.php">Contact Us</a>
                      </li>
                  </ul>
              </div>
          </div>
          <div class="mega-menu-submenu-ad-wrapper">
            <div
              class="eclipsefdn-mega-menu-promo-content mega-menu-promo-content"
              data-ad-format="ads_square"
              data-ad-publish-to="eclipse_org_home"
            >
            </div>
          </div>
      </div>
  </div>
  <?php endif ?>
  <?php print $this->getExtraHeaderHtml() ?>
  <style>
    /* @todo: Add these styles to EFSA */
    #header .header-toolbar .toolbar-link,
    #header .header-toolbar-row > .dropdown.open .dropdown-toggle {
      color: #333;
    }
  </style>
</header>
