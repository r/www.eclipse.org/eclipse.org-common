<?php
/**
 * Copyright (c) 2023 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Christopher Guindon (Eclipse Foundation) - Initial implementation
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

$navigation = $this->getNav();
if (!empty($navigation['#items'])) :
?>
<aside <?php print $this->getAttributes('main-sidebar'); ?>>
  <ul class="ul-left-nav" id="leftnav" role="tablist" aria-multiselectable="true">
    <?php foreach ($navigation['#items'] as $key => $link) :?>
      <?php /* Headings */?>
      <?php if ($link['item']->getTarget() == "__SEPARATOR") :?>
        <li class="main-sidebar-main-item main-sidebar-item-indented separator">
          <a 
            class="main-sidebar-heading link-unstyled"
            <?php if ($link['item']->getURL()) :?>
              href="<?php $link["item"]->getURL(); ?>"
            <?php endif; ?>
          >
            <?php print $link['item']->getText(); ?>
          </a>
        </li>
      <?php elseif (!empty($navigation['#items'][$key]['children'])): ?>
        <li class="main-sidebar-item main-sidebar-item-indented">
          <a
            class="link-unstyled"
            role="button"
            href="#sidebar-item-<?php print $key; ?>"
            data-toggle="collapse"
            aria-expanded="false"
            aria-controls="sidebar-item-<?php print $key; ?>"
          >
            <span><?php print $link['item']->getText(); ?></span>
            <i class="main-sidebar-item-icon fa fa-chevron-down" aria-hidden="true"></i>
          </a>
          <div class="collapse" id="sidebar-item-<?php print $key; ?>">
            <ul class="main-sidebar-item-submenu list-unstyled">
              <?php foreach ($navigation['#items'][$key]['children'] as $child): ?>
                <li class="submenu-item">
                  <a 
                    class="link-unstyled" 
                    href="<?php print $child->getURL(); ?>"
                    target="<?php print ($child->getTarget() == "_blank") ? "_blank" : "_self"; ?>"
                  >
                    <?php print $child->getText() ?>
                  </a>
                </li>
              <?php endforeach; ?>
            </ul>
          </div>
        </li>
      <?php else :?>
        <li class="main-sidebar-item main-sidebar-item-indented">
          <a class="link-unstyled" href="<?php print $link['item']->getURL() ?>">
            <?php print $link['item']->getText(); ?>
          </a>
        </li>
      <?php endif; ?>
    <?php endforeach; ?>
    <?php if (!empty( $navigation['html_block'])) :?>
      <div<?php print $this->getAttributes('main-sidebar-html-block');?>>
        <?php print $navigation['html_block']; ?>
      </div>
    <?php endif;?>
  </aside>
  <?php print $navigation['html_block_suffix']; ?>
<?php endif;?>