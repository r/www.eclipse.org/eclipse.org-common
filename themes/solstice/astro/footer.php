<?php
/**
 * Copyright (c) 2023 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Christopher Guindon (Eclipse Foundation) - Initial implementation
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<p id="back-to-top">
  <a class="visible-xs" href="#top">Back to the top</a>
</p>
<?php if($this->getDisplayFooterPrefix()): ?>
  <?php print $this->getFooterPrexfix();?>
<?php endif; ?>
<footer id="footer">
  <div class="container">
    <div class="footer-sections row equal-height-md font-bold">
      <div id="footer-eclipse-foundation" class="footer-section col-md-5 col-sm-8">
        <?php print $this->getFooterRegion1(); ?>
      </div>
      <div id="footer-legal" class="footer-section col-md-5 col-sm-8">
        <?php print $this->getFooterRegion2(); ?>
      </div>
      <div id="footer-more" class="footer-section col-md-5 col-sm-8">
        <?php print $this->getFooterRegion3(); ?>
      </div>
      <div id="footer-end" class="footer-section col-md-8 col-md-offset-1 col-sm-24">
        <div class="footer-end-social-container">
          <div class="footer-end-social">
            <p class="footer-end-social-text">Follow Us:</p>
            <ul class="footer-end-social-links list-inline">
              <a class="link-unstyled" href="https://www.youtube.com/channel/UCej18QqbZDxuYxyERPgs2Fw" title="YouTube Channel">
              <span class="fa fa-stack">
              <i class="fa fa-circle fa-stack-2x" aria-hidden="true"></i>
              <i class="fa fa-youtube-play fa-stack-1x fa-inverse" aria-hidden="true"></i>
              </span>
              </a>
              <a class="link-unstyled" href="https://www.linkedin.com/company/eclipse-foundation/" title="LinkedIn">
              <span class="fa fa-stack">
              <i class="fa fa-circle fa-stack-2x" aria-hidden="true"></i>
              <i class="fa fa-linkedin fa-stack-1x fa-inverse" aria-hidden="true"></i>
              </span>
              </a>
              <a class="link-unstyled" href="https://www.facebook.com/eclipse.org/" title="Facebook">
              <span class="fa fa-stack">
              <i class="fa fa-circle fa-stack-2x" aria-hidden="true"></i>
              <i class="fa fa-facebook fa-stack-1x fa-inverse" aria-hidden="true"></i>
              </span>
              </a>
              <a class="link-unstyled" href="https://twitter.com/EclipseFdn" title="Twitter">
              <span class="fa fa-stack">
              <i class="fa fa-circle fa-stack-2x" aria-hidden="true"></i>
              <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
              </span>
              </a>
            </ul>
          </div>
        </div>
        <div class="footer-end-newsletter">
          <div class="footer-end-newsletter">
            <form id="mc-embedded-subscribe-form" action="https://eclipsecon.us6.list-manage.com/subscribe/post" method="post" novalidate target="_blank">
                <label class="footer-end-newsletter-label" for="email">Subscribe to our Newsletter</label>
                <div class="footer-end-newsletter-input-wrapper">
                    <input class="footer-end-newsletter-input" type="email" id="email" name="EMAIL" autocomplete="email" placeholder="Enter your email address" />
                    <div>
                        <i class="fa fa-solid fa-envelope fa-lg" aria-hidden="true"></i>
                    </div>
                </div>
                <input type="hidden" name="u" value="eaf9e1f06f194eadc66788a85" />
                <input type="hidden" name="id" value="46e57eacf1" />
                <input id="mc-embedded-subscribe" type="submit" name="subscribe" hidden />
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-24">
      <div class="row">
        <div id="copyright" class="col-md-16">
        <p id="copyright-text"><?php print $this->getCopyrightNotice();?></p>
        </div>
      </div>
    </div>
    <a href="#" class="scrollup">Back to the top</a>
  </div>
</footer>
<!-- Placed at the end of the document so the pages load faster -->
<script<?php print $this->getAttributes('script-theme-main-js');?>></script>
<?php print $this->getExtraJsFooter();?>
</body>
</html>
