/*!
 * Copyright (c) 2018, 2019, 2020, 2021 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Christopher Guindon <chris.guindon@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require('./node_modules/eclipsefdn-solstice-assets/webpack-solstice-assets.mix');
let mix = require('laravel-mix');
mix.EclipseFdnSolsticeAssets();

mix.setPublicPath('public');
mix.setResourceRoot('../');

// Default CSS
mix.less(
  'node_modules/eclipsefdn-solstice-assets/less/quicksilver/eclipse-ide/styles.less',
  'public/stylesheets/eclipse-ide.min.css'
);
mix.less(
  'node_modules/eclipsefdn-solstice-assets/less/quicksilver/styles.less',
  'public/stylesheets/quicksilver.min.css'
);

mix.less(
  'node_modules/eclipsefdn-solstice-assets/less/astro/main.less',
  'public/stylesheets/astro.min.css'
);

mix.less(
  'node_modules/eclipsefdn-solstice-assets/less/quicksilver/jakarta/styles.less',
  'public/stylesheets/jakarta.min.css'
);
mix.less(
  'node_modules/eclipsefdn-solstice-assets/less/solstice/_barebone/styles.less',
  'public/stylesheets/barebone.min.css'
);
mix.less(
  'node_modules/eclipsefdn-solstice-assets/less/solstice/_barebone/footer.less',
  'public/stylesheets/barebone-footer.min.css'
);
mix.less(
  'node_modules/eclipsefdn-solstice-assets/less/solstice/table.less',
  'public/stylesheets/table.min.css'
);

//mix.less('node_modules/eclipsefdn-solstice-assets/less/solstice/forums.less', 'public/stylesheets/forums.min.css');
//mix.less('node_modules/eclipsefdn-solstice-assets/less/solstice/styles.less', 'public/stylesheets/styles.min.css');

// Copy cookieconsent files
mix.copy(
  'node_modules/cookieconsent/build/cookieconsent.min.css',
  'public/stylesheets/vendor/cookieconsent/cookieconsent.min.css'
);
mix.copy(
  'node_modules/cookieconsent/build/cookieconsent.min.js',
  'public/javascript/vendor/cookieconsent/cookieconsent.min.js'
);
mix.js(
  'js/cookieconsent.js',
  'public/javascript/vendor/cookieconsent/default.min.js'
);

// Copy eclipsefdn videos file
mix.less(
  'node_modules/eclipsefdn-solstice-assets/less/_components/eclipsefdn-video.less',
  'public/stylesheets/eclipsefdn-video.min.css'
);
mix.js('js/eclipsefdn.videos.js', 'public/javascript/eclipsefdn.videos.min.js');

// Astro layout only
mix.js('js/barebone-layout-astro.js', 'public/javascript/barebone-layout-astro.min.js');

// Main JavaScript
mix.js('js/main.js', 'public/javascript/main.min.js');
mix.js('js/main-astro.js', 'public/javascript/astro.min.js');
mix.js('js/barebone.js', 'public/javascript/barebone.min.js');
